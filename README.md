<p align="center"><img src="https://img.shields.io/badge/php-%23777BB4.svg?&style=for-the-badge&logo=php&logoColor=white"/> <img src="https://img.shields.io/badge/laravel%20-%23FF2D20.svg?&style=for-the-badge&logo=laravel&logoColor=white"/></p>

# Installation Notes:
- Clone the Repository
- Run: composer install
- Run: npm install
- Make a copy of .env.example file [cp .env.example .env]

Note: Create and empty database and run migrations and seeders.