<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/update', function () {
    return view('updateprod');
});
Route::get('/product', function () {
    return view('products');
});
*/

Auth::routes();

Route::get('/', function(){
    return \Redirect::route('login');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('/p', App\Http\Controllers\ProductController::class);
