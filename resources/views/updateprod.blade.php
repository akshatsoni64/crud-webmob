<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Update Product</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="card" style="margin-left:25%;margin-right:25%">
                <div class="card-header text-center" style="background: linear-gradient(90deg, rgba(6,207,219,1) 0%, rgba(4,181,223,1) 50%, rgba(2,14,222,1) 100%);">
                    <h4 class="text-white font-weight-bold">Product Information</h4>
                </div>
                <form action="#">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="md-form">
                                    <label for="sku">SKU</label>
                                    <input type="text" id="sku" class="form-control" placeholder="SKU">
                                </div>
                            </div>
                            <div class="col">
                                <div class="md-form">
                                    <input type="text" id="form1" class="form-control">
                                    <label for="form1">Product Name</label>
                                </div>
                            </div>
                            <div class="col">
                                <select name="category" class="mt-4 border-top-0 border-left-0 border-right-0 custom-select">
                                    <option selected>Category</option>
                                    <option value="volvo">Volvo</option>
                                    <option value="fiat">Fiat</option>
                                    <option value="audi">Audi</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="md-form">
                                    <input type="number" id="quantity" class="form-control">
                                    <label for="form1">Quantity</label>
                                </div>
                            </div>
                            <div class="col">
                                <div class="md-form">
                                    <input type="number" id="price" class="form-control">
                                    <label for="form1">Price</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label border-top-0 border-left-0 border-right-0" for="customFile">Add Product Image</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="card-footer">
                    <input type="submit" value="Update" class="btn btn-outline-success">
                    <input type="reset" value="Reset" class="ml-3 btn btn-outline-danger">
                </div>
            </div>
        </div>
    </body>
</html>