<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Product Page</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <style>
            h1{
                background: -webkit-linear-gradient(180deg, rgba(6,219,52,1) 0%, rgba(4,223,77,1) 50%, rgba(2,222,190,1) 100%);
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid mb-5">
            <h1 class="text-center display-2">Products</h1>
            <div class="row pt-5 px-3 border border-bottom-0 border-left-0 border-right-0">
                <div class="col">                 
                    <div class="card shadow rounded-lg">
                        <center><img style="margin-top:-3%" class="shadow text-center border border-info rounded-lg" src="https://www.joinusonline.net/pub/media/catalog/product/t/s/tshirt_v_neck_royal_blue_magic_1_1.jpg" height="300" width="250"></center>
                        <div class="card-body pt-4 text-center">
                            <div class="row">
                                <div class="col-7">
                                    <h3>Top</h3>
                                    <h3 class="text-primary font-weight-bold">V-Neck Tee</h3>                                    
                                </div>
                                <div class="col-5">
                                    <button class="float-right m-3 btn btn-primary rounded-circle" style="width:auto;">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>                                    
                                </div>
                            </div>
                        </div>
                        <div class="mx-4 p-2 border border-bottom-0 border-left-0 border-right-0">
                            <div class="row pt-2 text-center">
                                <div class="col">
                                    <h5>Quantity: 80</h5>
                                </div>
                                <div class="col">
                                    <h5>&#8377;700/-</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">                 
                    <div class="card shadow rounded-lg">
                        <center><img style="margin-top:-3%" class="shadow text-center border border-info rounded-lg" src="https://www.joinusonline.net/pub/media/catalog/product/t/s/tshirt_v_neck_royal_blue_magic_1_1.jpg" height="300" width="250"></center>
                        <div class="card-body pt-4 text-center">
                            <div class="row">
                                <div class="col-7">
                                    <h3>Top</h3>
                                    <h3 class="text-primary font-weight-bold">V-Neck Tee</h3>                                    
                                </div>
                                <div class="col-5">
                                    <button class="float-right m-3 btn btn-primary rounded-circle" style="width:auto;">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>                                    
                                </div>
                            </div>
                        </div>
                        <div class="mx-4 p-2 border border-bottom-0 border-left-0 border-right-0">
                            <div class="row pt-2 text-center">
                                <div class="col">
                                    <h5>Quantity: 80</h5>
                                </div>
                                <div class="col">
                                    <h5>&#8377;700/-</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">                 
                    <div class="card shadow rounded-lg">
                        <center><img style="margin-top:-3%" class="shadow text-center border border-info rounded-lg" src="https://www.joinusonline.net/pub/media/catalog/product/t/s/tshirt_v_neck_royal_blue_magic_1_1.jpg" height="300" width="250"></center>
                        <div class="card-body pt-4 text-center">
                            <div class="row">
                                <div class="col-7">
                                    <h3>Top</h3>
                                    <h3 class="text-primary font-weight-bold">V-Neck Tee</h3>                                    
                                </div>
                                <div class="col-5">
                                    <button class="float-right m-3 btn btn-primary rounded-circle" style="width:auto;">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>                                    
                                </div>
                            </div>
                        </div>
                        <div class="mx-4 p-2 border border-bottom-0 border-left-0 border-right-0">
                            <div class="row pt-2 text-center">
                                <div class="col">
                                    <h5>Quantity: 80</h5>
                                </div>
                                <div class="col">
                                    <h5>&#8377;700/-</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>