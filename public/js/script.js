$(document).ready(function(){
    $(".updtBtn").on('click',function(){
        $("#updtproduct").modal('toggle');
        var id = (this.id).substring(4, 5);
        // alert("Update Triggered for "+id);
        $.ajax({
            "url":"p/"+id,
            "type":"GET",
            success:function(data){
                $("#u_pname").val(data[0].pname);
                $("#u_category").val(data[0].category);
                $("#u_quantity").val(data[0].quantity);
                $("#u_price").val(data[0].price);
                $("#updateForm").attr("action", "p/"+id);
                // var file = new File(['name'], data[0].image);
                // var files_ob = new DataTransfer();
                // files_ob.items.add(file);
                // $("#u_image")[0].files = files_ob.files;
                // console.log(file);
            }
        });
    });
});