<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'id' => '1',
                'sku' => '1',
                'pname' => 'Black Cocktail',
                'category' => 'cocktail',
                'price' => '270',
                'quantity' => '50',
                'image' => 'prod_img/1.png',
            ],[
                'id' => '2',
                'sku' => '2',
                'pname' => 'Red Cocktail',
                'category' => 'cocktail',
                'price' => '270',
                'quantity' => '50',
                'image' => 'prod_img/2.png',
            ],[
                'id' => '3',
                'sku' => '3',
                'pname' => 'Black Gown',
                'category' => 'gown',
                'price' => '2700',
                'quantity' => '25',
                'image' => 'prod_img/3.png',
            ],[
                'id' => '4',
                'sku' => '4',
                'pname' => 'Green Gown',
                'category' => 'gown',
                'price' => '2700',
                'quantity' => '30',
                'image' => 'prod_img/4.png',
            ],[
                'id' => '5',
                'sku' => '5',
                'pname' => 'Short Shirt',
                'category' => 'shirt',
                'price' => '500',
                'quantity' => '70',
                'image' => 'prod_img/5.png',
            ],[
                'id' => '6',
                'sku' => '6',
                'pname' => 'Short Shirt',
                'category' => 'shirt',
                'price' => '550',
                'quantity' => '55',
                'image' => 'prod_img/6.png',
            ],
        ]);
    }
}
